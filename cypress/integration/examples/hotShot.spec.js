import datas from '../../fixtures/functionalData.json';

describe('HotShot application functional tests of user creation', () => {
  datas.forEach((data) => {
    context(`viewport: ${data.viewport}`, () => {
      beforeEach(() => {
        cy.viewport('macbook-15');
        cy.visit({
          url: '/customer/dashboard',
        });
        cy.get('[aria-label="Email"]').type(Cypress.env("username"));
        cy.get('[type="password"]').type(Cypress.env("password"));
        cy.get('.q-btn__wrapper').click();
        cy.wait(30000);
      });
      it('should display today and overview section with details in dashboard page ', () => {
        cy.get('.self-center.full-width.no-outline')
          .find('.dashboard-subtitle')
          .should('have.text', 'Today');
        data.Today.forEach((Todays, index) => {
          cy.get('.col-4.col-md-3').as('todays');
          cy.get('@todays')
            .find('.overview-text')
            .eq(index)
            .should('have.text', Todays.displayName);
            cy.get('@todays').find('.dashboard-overview-value').eq(index).should('exist')
        });
        cy.get('.hotshot-dashboard-page')
          .find('.dashboard-subtitle.q-pt-lg')
          .should('have.text', 'Overview');
        cy.get('.q-field__control')
          .findByText('Property type')
          .should('be.visible');
        cy.get(
          '.q-field__append.q-field__marginal.row.no-wrap.items-center.q-anchor--skip'
        ).as('button');
        cy.get('@button').find('[role="presentation"]').first().click();
        cy.get('#qvs_1 > div').should('have.length', '3');
        cy.get('#qvs_1 > div').each(($el, index) => {
          cy.get($el).should('have.text', data.Overview.popertyType[index]);
        });
        cy.get('@button').find('[role="presentation"]').first().click();
        cy.get('.overview-box')
          .find('.overview-title')
          .each(($el, index) => {
            cy.get($el)
              .should('contain.text', data.Overview.overviewbox[index])
              .should('be.visible');
          });
      });
      it('should create a user group ', () => {
        cy.get('.dash-menu-item').each(($el) => {
          cy.get($el)
            .invoke('text')
            .then((displayImageSrc) => {
              const a = displayImageSrc.replace(' ', '').trim();
              cy.log(a);
              if (a === 'Groups') {
                cy.get($el).click();
                cy.get('.top-menu-button').click();
                cy.get('[aria-label="Name"]').type(data.Group.Name);
                cy.get('[aria-label="Email"]').type(data.Group.Email);
                cy.get('.block').click();
                cy.findByText("I'll do this later").click();
                cy.get('[data-cy="portal-id"]').click();
                cy.get('.q-item__label').should('have.text', data.Group.Portal);
                cy.get('.q-item__label').click();
                cy.findByText('Finish').click();
                cy.wait(1000);
                cy.get('.q-table')
                  .get('tbody')
                  .find('tr')
                  .each(($el, index) => {
                    cy.log($el);
                    cy.get($el)
                      .get('td')
                      .eq(index)
                      .invoke('text')
                      .then((text) => {
                        const b = text;
                        cy.log(b);
                        if (b === data.Group.Name) {
                          cy.get($el)
                            .get('td')
                            .eq(index)
                            .should('have.text', data.Group.Name);
                          cy.get($el)
                            .get('td')
                            .eq(3)
                            .should('have.text', 'draft');
                        }
                      });
                  });
              }
            });
        });
      });
      it('should create a property ', () => {
       cy.get('.dash-menu-item').each(($el) => {
             cy.get($el)
               .invoke('text')
               .then((displayImageSrc) => {
                 const a = displayImageSrc.replace(' ', '').trim();
                 cy.log(a);
                 if (a === 'Properties') {
                   cy.get($el).click();
                   cy.get('.top-menu-button').click();
                   cy.get('[data-cy="group-id"]').click();
                   cy.wait(1000);
                   cy.get('.q-item__section > div').each(($el) => {
                     cy.get($el)
                       .invoke('text')
                       .then((tx) => {
                       cy.log(tx)
                         if (tx === data.Group.Name) {
                           cy.get($el).click();
                         }
                       });
                   });
                   cy.get(
                     '.q-item__section.column.q-item__section--main.justify-center'
                   ).as('select');
                   cy.get('[data-cy="property-name"]').type(
                     data.Properties.PropertyName
                   );
                   cy.get('[data-cy="property-type"]').type(
                     data.Properties.PropertyType
                   );
                   cy.get('@select').findByText('Hotel').click();
                   cy.get('[data-cy="external-reference"]').type(
                     data.Properties.ExternalReference
                   );
                   cy.get('[data-cy="package-id"]').click();
                   cy.get('@select')
                     .findByText(data.Properties.DefaultPackage)
                     .click();
                   cy.get('[data-cy="send-email"]').click();
                   cy.get('[data-cy="send-sms"]').click();
                   cy.get('[data-cy="street-address"]').type(
                     data.Properties.StreetAddress
                   );
                   cy.get('[data-cy="city"]').type(data.Properties.City);
                   cy.get('[data-cy="zip-code"]').type(data.Properties.ZipCode);
                   cy.get('[data-cy="country"]').first().click();
                   cy.get('[data-cy="country"]').first().type('Sou')
                   cy.wait(1000)
                    cy.get('.q-item__label').each(($el) => {
                  cy.get($el)
                                .invoke('text')
                                .then((tx) => {
                                cy.log(tx)
                                  if (tx === data.Properties.Country) {
                                    cy.get($el).click();
                                  }
                                });
                            });
                   cy.get('[data-cy="email"]').type(data.Properties.Email);
                   cy.get('.q-stepper__nav').find('[role="button"]').click();
                   cy.get('[data-cy="mac-address"]').type(
                     data.Properties.GMacAddress
                   );
                   cy.get('[data-cy="ip-address"]').type(
                     data.Properties.GIPadress
                   );
                   cy.get('[data-cy="dns-name"]').type(data.Properties.DNSname);
                   cy.get('[data-cy="username"]').type(data.Properties.GLogin);
                   cy.get('[data-cy="password"]').type(data.Properties.GPassord);
                   cy.get('[data-cy="gateway-type-id"]').click();
                   cy.get('.q-item__label').click();
                   cy.get(
                     '.finish-button-black > .q-btn__wrapper > .q-btn__content'
                   ).as('finishButton');
                   cy.get('@finishButton').click();
                   cy.get('[data-cy="property-interface-type-id"]').click();
                   cy.get('@select').findByText('FIAS').click();
                   cy.get('[data-cy=host]').type(data.Properties.IHost);
                   cy.get('[data-cy=package-id]').click();
                   cy.get('.q-item').click();
                   cy.get('[data-cy=port]').type(data.Properties.IPort);
                   cy.get('[data-cy=sales-outlet]').type(
                     data.Properties.ISalesOutlet
                   );

                   cy.get('@finishButton').click();

                   cy.get('[data-cy="portal-id"]').click();
                   cy.get('@select').findByText('IO Digital').click();
                   cy.get('@finishButton').click();
                   cy.wait(1000);
                   cy.get('.q-table')
                     .get('tbody')
                     .find('tr')
                     .each(($el, index) => {
                       cy.log($el);
                       cy.get($el)
                         .get('td')
                         .eq(index)
                         .invoke('text')
                         .then((text) => {
                           const b = text;
                           cy.log(b);
                           if (b === data.Properties.PropertyName) {
                             cy.get($el)
                               .get('td')
                               .eq(index)
                               .should('have.text', data.Properties.PropertyName);
                             cy.get($el)
                               .get('td')
                               .eq(3)
                               .should('have.text', 'active');
                           }
                         });
                     });
                 }
               });
           });
         });      it('should delete a property ', () => {
        cy.get('.dash-menu-item').each(($el) => {
          cy.get($el)
            .invoke('text')
            .then((page) => {
              const a = page.replace(' ', '').trim();
              cy.log(a);
              if (a === 'Properties') {
                cy.get($el).click();
                ///Deleting created property
                cy.get('.q-table')
                  .get('tbody')
                  .find('tr')
                  .each(($el, index) => {
                    cy.log($el);
                    cy.get($el)
                      .get('td')
                      .eq(index)
                      .invoke('text')
                      .then((text) => {
                        const b = text;
                        cy.log(b);
                        if (b === data.Properties.PropertyName) {
                          cy.get($el)
                            .get('td')
                            .eq(index)
                            .should('have.text', data.Properties.PropertyName);
                          cy.get($el)
                            .get('td')
                            .eq(3)
                            .should('have.text', 'active');
                          cy.wait(1000);
                          cy.get($el)
                            .get('td')
                            .get('[role="link"]')
                            .first()
                            .click();
                          cy.wait(1000);
                          cy.get(
                            '.q-btn__content.text-center.col.items-center.q-anchor--skip.justify-center.row'
                          )
                            .get('[class="block"]')
                            .each(($el) => {
                              cy.get($el)
                                .invoke('text')
                                .then((button) => {
                                  const deleteb = button;
                                  cy.log(deleteb);
                                  if (deleteb === 'Delete property') {
                                    cy.get($el).click();
                                    cy.wait(1000);
                                   //  cy.get('.q-card__actions.bg-white.q-card__actions--vert.column.items-center').get('[role="button"]').click({multiple: true},{ force: true })
                                  }
                                });
                            });
                          cy.get('.text-center.q-card')
                            .find('[class="block"]')
                            .each(($el) => {
                              cy.get($el)
                                .invoke('text')
                                .then((button) => {
                                  const deleteb = button;
                                  cy.log(deleteb);
                                  if (deleteb === 'Delete Property') {
                                    cy.get($el).click();
                                    cy.wait(1000);
                                  }
                                });
                            });
                        }
                      });
                  });
              }
            });
        });
      });
    });
  });
});
